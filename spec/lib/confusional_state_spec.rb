# frozen_string_literal: true

require "spec_helper"
require "confusional_state"

RSpec.describe ConfusionalState do
  subject(:confusional_state) { described_class }

  describe "#start" do
    before do
      # Skip zookeepr connection....
      allow(Sync::Conf).to receive(:new)
    end

    it "expect to initialize App" do
      app = instance_double("App", { "start": nil })
      expect(App).to receive(:new).and_return(app)
      ConfusionalState.start
    end

    it "expect to start App" do
      expect_any_instance_of(App).to receive(:start)
      ConfusionalState.start
    end
  end
end

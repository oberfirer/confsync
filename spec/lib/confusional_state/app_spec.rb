# frozen_string_literal: true

require "spec_helper"
require "boot_helper"

RSpec.describe App do
  subject(:app) { described_class }

  describe "#new" do
    it "expect to initialize Sync" do
      expect(Sync::Conf).to receive(:new)
      app.new
    end

    it "expect to have default thread safe config" do
      expect(app.config).to be_a_kind_of(Dry::Configurable::Config)
    end

    it "expect to have hash like config" do
      expect(app.config.values).to be_a_kind_of(Hash)
    end

    it "expect to have default config values" do
      app.config.values.each do |key, value|
        expect(value).to be >= 0
      end
    end
  end
end

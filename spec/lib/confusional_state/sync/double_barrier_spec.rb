# frozen_string_literal: true

require "spec_helper"
require "boot_helper"

RSpec.describe Sync::DoubleBarrier do
  subject(:barrier_class) { described_class }

  let(:zk) { double("zk") }
  let(:ready) { double("ready") }
  let(:closed) { double("closed") }
  let(:barrier) { barrier_class.new(zk, ready, closed) }

  describe "#join" do
    it "expect to return if current Process is not on require list" do
      expect(barrier).to receive(:current_required?).and_return(false)
      expect(barrier).to_not receive(:process_joined?)
      expect(barrier).to_not receive(:init_with_timeout).with(any_args)

      barrier.join
    end

    it "expect to return if current Process already joined" do
      expect(barrier).to receive(:current_required?).and_return(true)
      expect(barrier).to receive(:process_joined?).and_return(true)
      expect(barrier).to_not receive(:init_with_timeout).with(any_args)

      barrier.join
    end

    it "expect to join if meet conditions" do
      expect(barrier).to receive(:current_required?).and_return(true)
      expect(barrier).to receive(:process_joined?).and_return(false)
      expect(barrier).to receive(:init_with_timeout).with(any_args)

      barrier.join
    end
  end

  describe "#init_barrier" do
    it "expect to not add ready node" do
      expect(barrier).to receive(:watch_ready)
      expect(barrier).to receive(:add_proceess_node)
      expect(barrier).to receive(:all_joined?).and_return(false)
      expect(barrier).to_not receive(:add_ready_node)

      barrier.init_barrier
    end

    it "expect to add ready node" do
      expect(barrier).to receive(:watch_ready)
      expect(barrier).to receive(:add_proceess_node)
      expect(barrier).to receive(:all_joined?).and_return(true)
      expect(barrier).to receive(:add_ready_node)

      barrier.init_barrier
    end
  end

  describe "#process_joined?" do
    it "expect to check zookeeper path existance" do
      expect(zk).to receive(:exists?).with(Sync::Paths::BARRIER_PROCESS)
      barrier.process_joined?
    end
  end

  describe "#current_required?" do
    it "expect to check zookeeper required path" do
      expect(zk).to receive(:get).and_return([Sync::Paths::BARRIER_PROCESS.to_json])
      expect(barrier.current_required?).to be(true)
    end
  end

  describe "#create_required?" do
    it "expect to check zookeeper required path" do
      list = ["p100"]
      expect(zk).to receive(:children).with(Sync::Paths::REGISTERED).and_return(list)
      expect(zk).to receive(:create).with(Sync::Paths::BARRIER_REQUIRED, data: list.to_json)
      barrier.create_required
    end
  end

  describe "#create?" do
    it "expect to check zookeeper required path" do
      expect(zk).to receive(:create).with(Sync::Paths::BARRIER, mode: :persistent, data: Sync::Paths::PROCESS_NAME)
      barrier.create
    end
  end
end

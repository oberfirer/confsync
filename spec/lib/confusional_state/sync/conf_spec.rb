require "spec_helper"
require "boot_helper"

RSpec.describe Sync::Conf do
  subject(:conf) { described_class }

  # TODO Refactor Conf class. Now wee need to stub all initialize to skip zk connections anch checks
  before do
    allow_any_instance_of(conf).to receive_messages({
      zk_defaults: true,
      connect: true,
      create_base_paths: true,
      barrier_watcher: true,
      register: true,
      sync_app_config: true,
    })
  end

  describe "#update_config" do
    it "should store config on update" do
      cfg = { foo: 1, bar: 2, baz: 3 }
      sync = conf.new(cfg)
      expect(sync).to receive(:store_new_config).with(cfg)
      expect(sync).to receive(:update!)
      sync.update_config(cfg)
    end
  end
end

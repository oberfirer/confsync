require "zeitwerk"
require "pry"
# TODO Load dev dependencies only in dev environment

module ConfusionalState
  def self.boot!
    Zeitwerk::Loader.new
      .tap { |l| l.push_dir(File.absolute_path(File.join(File.dirname(__FILE__), "confusional_state"))) }
      .tap(&:setup)
      .tap(&:eager_load)
  end

  def self.start
    @app = App.new
    @app.start
  end
end

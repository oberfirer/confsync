module Utils
  class QueueWithTimeout
    def initialize(&timeout_callback)
      @timeout_callback = timeout_callback
      @mutex = Mutex.new
      @queue = []
      @received = ConditionVariable.new
    end

    def <<(x)
      @mutex.synchronize do
        @queue << x
        @received.signal
      end
    end

    def pop(timeout = 15)
      @mutex.synchronize do
        if @queue.empty? && timeout != 0
          timeout_time = timeout + Time.now.to_f
          while @queue.empty? && (remaining_time = timeout_time - Time.now.to_f) > 0
            @received.wait(@mutex, remaining_time)
          end
        end
        if @queue.empty?
          @timeout_callback.call
        else
          @queue.shift
        end
      end
    end
  end
end

# frozen_string_literal: true

# ZooKeepr Client
#   Keeps connection
#   Keeps configuration in sync

# zookeeper
# kamilapp
#   registered
#     p543
#     p6456
#     p3465
#   conf (json with app config)
#   barrier_ready
#   barrier_required (json with required processes node names)
#   barrier
#     p543
#     p6456
#     p3465

require "zk"
require "json"
require "yaml"
require "time"

module Sync
  class Conf
    prepend Instrumentation::AppNotifier

    ZK_CONFIG_FILE = File.expand_path("conf/zookeeper.yml")
    TIMEOUT = 15

    private_constant :TIMEOUT, :ZK_CONFIG_FILE

    ZK::Client::DEFAULT_TIMEOUT = TIMEOUT

    def initialize(config)
      @config = config

      zk_defaults
      connect
      create_base_paths
      barrier_watcher
      register
      sync_app_config
    end

    def update_config(new_config)
      store_new_config(new_config)
      update!
    end

    private

    def store_new_config(new_config)
      @new_config = new_config
      @new_config_timestamp = encode_time(Time.now)
    end

    # TODO Thread the update. Barrier timeout is 15sec......
    def update!
      @write_queue = Queue.new
      after_barrier_released(@write_queue) do
        begin
          @barrier.create_required
          @barrier.create
          @barrier.wait
        rescue ZK::Exceptions::NodeExists
          after_barrier_released(@write_queue) do
            update! unless new_is_older?
          end
        end
      end
    end

    def sync_app_config
      config = read_config
      if config
        __notify__ "Sync config with zookeeper entry on init."
        update_app_config(config)
      else
        __notify__ "There is no config in zookeeper, lets add one."
        update_config(@config.values)
      end
      __notify__ "[APP SYNCHROZNIED]: #{@config.values.inspect}"
    end

    # This is used only on init (process is starting)
    def read_config
      @read_queue = Queue.new
      after_barrier_released(@read_queue) do
        zk_config_read
      end
    end

    def update_app_config(config)
      @config.update(config)
    end

    def zk_defaults
      ZK.logger = Sync::ZkLogger.new
    end

    def create_base_paths
      create_path(Sync::Paths::REGISTERED)
      create_path(Sync::Paths::CONFIG)
      create_path(Sync::Paths::CONFIG_TIME)
    end

    def connection_path
      zk_cfg = YAML.load_file(ZK_CONFIG_FILE)
      File.join("#{zk_cfg["host"]}:#{zk_cfg["port"]}", Sync::Paths::APP)
    end

    def connect
      path = connection_path
      __notify__("Connecting to zookeeper with #{path}") do
        @zk = ZK.new(path)
      end
    end

    def create_path(path, mode = :persistent, ignore = :node_exists, data = nil)
      __notify__("Creating path: #{path} with mode: #{mode} and ignore: #{ignore}")
      opts = { mode: mode }
      opts.merge!(ignore: ignore) if ignore
      opts.merge!(data: data) if data
      @zk.create(path, opts)
    end

    def register
      # Register ephemeral node for process so it will be deleted when client disconnect from ZooKeeper
      __notify__("Registering current process to zookeeper with path: #{Sync::Paths::REGISTER}")
      create_path(Sync::Paths::REGISTER, :ephemeral)
      @registered = true
    end

    def zk_config_read
      begin
        JSON.parse(@zk.get(Sync::Paths::CONFIG).first)
      rescue JSON::ParserError
        nil
      end
    end

    # Create Serializer class wich will be ducktyped parse data before store or after take from ZK
    def encode_time(time)
      time.to_i * (10 ** 9) + time.nsec
    end

    def zk_set_config(config)
      __notify__("Setting new config in zk: #{config.inspect}")
      @zk.set(Sync::Paths::CONFIG, config.to_json)
      @zk.set(Sync::Paths::CONFIG_TIME, encode_time(Time.now).to_json)
    end

    def config_time
      JSON.parse(@zk.get(Sync::Paths::CONFIG_TIME).first)
    end

    # Read config on barrier READY
    def on_barrier_ready
      -> {
        @barrier_config = zk_config_read
        __notify__("Barrier ready callback: #{@barrier_config.inspect}")
      }
    end

    # Switch configs on barrier LEAVE
    def on_barrier_closed
      -> {
        __notify__("Barrier closed callback: #{@barrier_config.inspect}")
        update_app_config(@barrier_config)
      }
    end

    def barrier_node_created
      __notify__("Barrier node created callback started")

      initiator = @zk.get(Sync::Paths::BARRIER).first
      current_initiator = initiator == Sync::Paths::PROCESS_NAME

      __notify__("Barrier initiator is: #{initiator} and We are: #{Sync::Paths::PROCESS_NAME}")

      if current_initiator
        __notify__("Current process is a barrier initiator.")
        zk_set_config(@new_config)
      end

      @barrier.join
      @zk.stat(Sync::Paths::BARRIER, :watch => true)
    end

    def barrier_node_deleted
      __notify__("Barrier node deleted.")
      if barrier_exists?(true)
        __notify__("Barrier created before we can catch it. Lets join it.")
        @barrier.join
      end
      # Release the lock when barrier exist before we set watcher
      @read_queue.push(:unlock) if @read_queue
      @write_queue.push(:unlock) if @write_queue
      @barrier.release_init
    end

    def barrier_node_changed
      __notify__("Barrier node changed")
      unless barrier_exists?(true)
        @read_queue.push(:unlock_read) if @read_queue
      end
    end

    def barrier_exists?(watch = false)
      @zk.exists?(Sync::Paths::BARRIER, :watch => watch)
    end

    def barrier_watcher
      __notify__("Setting watch for barrier.")
      init_barrier
      register_watcher
      watch_barrier
    end

    def init_barrier
      @barrier = Sync::DoubleBarrier.new(@zk, on_barrier_ready, on_barrier_closed)
    end

    def register_watcher
      @zk.register(Sync::Paths::BARRIER) do |event|
        if event.node_created?
          barrier_node_created
        elsif event.node_deleted?
          barrier_node_deleted
        else
          barrier_node_changed
        end
      end
    end

    def watch_barrier
      @zk.stat(Sync::Paths::BARRIER, :watch => true)
    end

    def after_barrier_released(queue)
      if barrier_exists?
        __notify__("Writing/Reading config waits because of barrier.")
        queue.pop
        __notify__("Writing/Reading config continue because of barrier is gone.")
        yield
      else
        yield
      end
    end

    # We should notify app for update failed because newer is set and we are synchronized to it
    # Every update should do it not only retry ? I am not sure about it
    def new_is_older?
      current_timestamp = config_time
      (current_timestamp) && (@new_config_timestamp <= current_timestamp)
    end

    def __notify__(event)
      if block_given?
        __app_notify__("sync.conf", event: event) do
          yield
        end
      else
        __app_notify__("sync.conf", event: event)
      end
    end
  end
end

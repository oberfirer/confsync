# We can assign logger to ZK.logger but we want use our monitor rather than normal Logger class
module Sync
  class ZkLogger
    prepend Instrumentation::AppNotifier
    %w[info notify error warn debug].each do |method|
      define_method(method) do |log = false, &block|
        if block
          __app_notify__("zookeeper.event", event: block.call, process: Process.pid)
        else
          __app_notify__("zookeeper.event", event: log, process: Process.pid)
        end
      end
    end
  end
end

module Sync
  class DoubleBarrier
    prepend Instrumentation::AppNotifier

    def initialize(client, ready_clbk, closed_clbk)
      @client, @ready_clbk, @closed_clbk = client, ready_clbk, closed_clbk
      @zk = client
      @init_queue = Queue.new
    end

    def join
      return unless current_required?
      return if process_joined?
      init_with_timeout(15)
      __notify__("Joined")
    end

    def init_with_timeout(timeout)
      @timeout_queue = Utils::QueueWithTimeout.new(&on_timeout)
      t_timeout = Thread.new { @timeout_queue.pop(timeout) }
      t_barrier = Thread.new { init_barrier }
      until t_barrier.join(timeout)
        t_timeout.join
      end
    end

    def init_barrier
      watch_ready
      add_proceess_node
      add_ready_node if all_joined?
    end

    def process_joined?
      @zk.exists?(Sync::Paths::BARRIER_PROCESS)
    end

    def wait
      @init_queue.pop
    end

    def release_init
      @init_queue.push(:release) if @init_queue && @init_queue.empty?
    end

    def current_required?
      required = JSON.parse(@zk.get(Sync::Paths::BARRIER_REQUIRED).first)
      required.include?(Sync::Paths::PROCESS_NAME)
    end

    def create_required
      __notify__("Create barrier required path.")
      list = @zk.children(Sync::Paths::REGISTERED)
      @zk.create(Sync::Paths::BARRIER_REQUIRED, data: list.to_json)
    end

    def create
      __notify__("Create barrier path.")
      @zk.create(Sync::Paths::BARRIER, mode: :persistent, data: Sync::Paths::PROCESS_NAME)
    end

    private

    def watch_ready
      __notify__("Watch barrier ready node.")
      @ready_notifier = @zk.register(Sync::Paths::BARRIER_READY, &barrier_ready_event)
      @zk.stat(Sync::Paths::BARRIER_READY, watch: true)
    end

    def add_proceess_node
      __notify__("Create barrier process path.")
      @zk.create(Sync::Paths::BARRIER_PROCESS, mode: :persistent)
    end

    def all_joined?
      current = @zk.children(Sync::Paths::BARRIER)
      required = JSON.parse(@zk.get(Sync::Paths::BARRIER_REQUIRED).first)
      current.sort === required.sort
    end

    def add_ready_node
      __notify__("Process is last which joined barrier.")
      @zk.create(Sync::Paths::BARRIER_READY, ignore: :node_exists, mode: :persistent)
    end

    def on_ready_created
      __notify__("Ready node created.")
      watch_leaving
      resume_ready_watcher
      ready_callback
      delete_current
    end

    def watch_leaving
      @closing_notifier = @zk.register(Sync::Paths::BARRIER, &barrier_children_event)
      @zk.children(Sync::Paths::BARRIER, watch: true)
    end

    def resume_ready_watcher
      @zk.stat(Sync::Paths::BARRIER_READY, watch: true)
    end

    def on_ready_deleted
      __notify__("Ready node deleted.")
      cancel_timeout
      unsubscribe_watchers
      closed_callack
      delete_barrier_nodes
    end

    def closed_callack
      @closed_clbk.call
    end

    def ready_callback
      @ready_clbk.call
    end

    def cancel_timeout
      @timeout_queue << :cancel
    end

    def on_ready_changed
      @zk.stat(Sync::Paths::BARRIER_READY, watch: true)
    end

    def barrier_ready_event
      ->(event) {
        __notify__("Ready path event catched")
        if event.node_created?
          on_ready_created
        elsif event.node_deleted?
          on_ready_deleted
        else
          on_ready_changed
        end
      }
    end

    def barrier_children_event
      ->(event) {
        chlds = @zk.children(Sync::Paths::BARRIER, ignore: :no_node)
        unless chlds
          __notify__("Barrier is empty dont need to do anything.")
          @closing_notifier.unregister
        else
          __notify__("Watching barrier for empty state: #{chlds.inspect}")
          if chlds.size == 0
            __notify__("Deleting ready node path we are last removed from barrier.")
            @closing_notifier.unregister
            @zk.delete(Sync::Paths::BARRIER_READY, ignore: :no_node)
          else
            __notify__("Children event reached but still some nodes in barrier.")
            @zk.children(Sync::Paths::BARRIER, watch: true)
          end
        end
      }
    end

    def on_timeout
      -> {
        __notify__("15 sec timeout reached.")
        unsubscribe_watchers
        delete_current
        clean_orphaned_nodes
        clean_barrier_nodes
      }
    end

    def unsubscribe_watchers
      @ready_notifier.unsubscribe if @ready_notifier
      @closing_notifier.unsubscribe if @closing_notifier
    end

    def delete_current
      @zk.delete(File.join(Sync::Paths::BARRIER_PROCESS))
    end

    def delete_barrier_nodes
      initiator = @zk.get(Sync::Paths::BARRIER, ignore: :no_node).first
      should_delete = (initiator && initiator == Sync::Paths::PROCESS_NAME)
      if should_delete
        @zk.delete(Sync::Paths::BARRIER)
        @zk.delete(Sync::Paths::BARRIER_REQUIRED)
      end
    end

    # Clean barrier related nodes
    def clean_barrier_nodes
      in_barrier = @zk.children(Sync::Paths::BARRIER, ignore: :no_node)
      if (in_barrier && in_barrier.empty?)
        @zk.delete(Sync::Paths::BARRIER_REQUIRED, ignore: :no_node)
        @zk.delete(Sync::Paths::BARRIER_READY, ignore: :no_node)
        @zk.delete(Sync::Paths::BARRIER, ignore: :no_node)
      end
    end

    # Clean orphaned nodes (Process lost connection to zookeepr or just go down)
    def clean_orphaned_nodes
      in_barrier = @zk.children(Sync::Paths::BARRIER, ignore: :no_node)
      registered = @zk.children(Sync::Paths::REGISTERED, ignore: :no_node)
      orphaned = (in_barrier && !in_barrier.empty?) ? (registered - in_barrier) : false
      orphaned.each { |node| @zk.delete(File.join(Sync::Paths::BARRIER, node, ignore: :no_node)) } if orphaned
    end

    def __notify__(event)
      if block_given?
        __app_notify__("sync.barrier", event: event) do
          yield
        end
      else
        __app_notify__("sync.barrier", event: event)
      end
    end
  end
end

# frozen_string_literal: true
module Sync
  module Paths
    APP = "kamilapp"
    CONFIG = "/config"
    PROCESS_NAME = "p#{Process.pid}"
    REGISTERED = "/registered"
    REGISTER = File.join(REGISTERED, PROCESS_NAME)
    BARRIER = "/barrier"
    BARRIER_REQUIRED = "/barrier_required"
    BARRIER_READY = "/barrier_ready"
    BARRIER_PROCESS = File.join(BARRIER, PROCESS_NAME)
    CONFIG_TIME = "/config_time"
  end
end

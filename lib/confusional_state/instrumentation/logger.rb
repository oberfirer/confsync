require "logger"

module Instrumentation
  class Logger
    def initialize(dir)
      @process = ::Logger.new(File.join(dir, "#{Process.pid}.log"))
      @error = ::Logger.new(File.join(dir, "error.log"))
    end

    # Consider seprarating zk logs to anoother file
    def on_zookeeper_event(event)
      process_log("[ZooKeeper] event: #{event.payload[:event]}")
    end

    def on_process_log_configuration(event)
      process_log("[App] Configuration: #{event.payload[:config].values}")
    end

    def on_sync_conf(event)
      process_log("[Sync]: #{event.payload[:event]}")
    end

    def on_sync_barrier(event)
      process_log("[Barrier]: #{event.payload[:event]}")
    end

    def on_sync_timeout(event)
      error_log(event)
    end

    def on_sync_failed(event)
      error_log(event)
    end

    def on_process_update_configuration(event)
      process_log("############################################################################")
      process_log("############################################################################")
      process_log("Updating configuration: configs: #{event.inspect} ")
      process_log("############################################################################")
      process_log("############################################################################")
    end

    private

    def process_log(entry)
      @process.info(entry)
    end

    def error_log(entry)
      @error.error(entry)
    end
  end
end

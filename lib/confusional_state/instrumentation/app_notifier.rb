# frozen_string_literal: true

require "dry/monitor/notifications"

module Instrumentation
  module AppNotifier
    LOGGER_DIR = File.expand_path(File.join(__FILE__, "../../../../logs"))
    EVENTS = %w[
      zookeeper.event
      process.log_configuration
      process.update_configuration
      sync.conf
      sync.barrier
      sync.timeout
      sync.fail
    ]

    private_constant :LOGGER_DIR, :EVENTS

    def self.prepended(klass)
      klass.prepend(InstanceMethods)
    end

    def self.notifications
      unless @notifications
        @notifications = Dry::Monitor::Notifications.new(:my_app)
        EVENTS.each { |e| @notifications.register_event(e) }
        @notifications.subscribe(Instrumentation::Logger.new(LOGGER_DIR))
      end
      @notifications
    end

    def self.instrument(event, payload = {})
      @notifications.instrument(event, payload)
    end

    module InstanceMethods
      def __app_notify__(event, payload = {})
        if block_given?
          AppNotifier.notifications.instrument(event, payload) do
            yield
          end
        else
          AppNotifier.notifications.instrument(event, payload)
        end
      end
    end
  end
end

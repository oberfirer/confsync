# frozen_string_literal: true

require "dry-configurable"

class App
  prepend Instrumentation::AppNotifier

  SETTINGS = %w(foo bar baz)
  private_constant :SETTINGS

  # Default App thread-safe configuration that will be synchronized across processes
  extend Dry::Configurable
  SETTINGS.each_with_index do |key, idx|
    setting key, (idx + 1) * 100
  end

  def self.settings
    SETTINGS
  end

  def initialize
    @sync = Sync::Conf.new(App.config)
  end

  # This method suppose to work with app config synchronized with other processes
  def start
    loop do
      #__app_notify__("process.log_configuration", config: config)
      if rand(10000) > 9998
        new_config = rand_config
        __app_notify__("process.update_configuration", config: App.config, new_config: new_config)
        update_config(new_config)
      end
    end
  end

  private

  def update_config(new_conf)
    @sync.update_config(new_conf)
  end

  def rand_config
    App.settings.map { |key| [key, rand(1000)] }.to_h
  end
end

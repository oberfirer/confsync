    
# Problem solution description:

Synchronization in-memory state (I will call it config later) between processes.

So perhaps we cannot read config from a solution like Redis because we want to avoid that few milliseconds every time we used configuration in our app.
My solution is to use Apache zookeeper which enables highly reliable distributed coordination.
Going further I implemented double barrier recipe described here: 

https://zookeeper.apache.org/doc/r3.1.2/recipes.html#sc_doubleBarriers

btw. There are ready implementations likes Apache Curator:

https://curator.apache.org/curator-recipes/double-barrier.html

# Requirements

ruby (tested on 2.7.1)  
zookeeper (tested on 3.6.1)

# Installation

1. Make sure zookeeper server is running
2. Setup config/zookeeper.yml with your zookeeper server configuration (default localhost:2181)
3. Bundle dependencies

```ruby
  bundle
```

# Running app

To spawn 5 processes that are running constantly and tries to update config (btw. they are spamming a lot of updates in every second !)

```ruby
  rake
```

# Stop app

to kill all processes and clear logs:

```ruby
  rake kill
```

# Important notes

* ### If any process is killed (or lost zk connection) or we kill all processes with rake kill:

The timeout functionality will work.  
Other processes will leave barrier without update  
They will also clean zk paths.  
Everything is still working. Processes will spawn updates and barrier will work as expected.  
Any new process can be spawned and will register to zk  
BUT  
If we kill all processes (like rake kill) no one will clean the zk paths  
So if we now start processes again (rake) there could be a barrier created not cleaned and no one will clean it up.  
This is on TODO list to implement  
For now You need to clean paths when all processes died and you want to run it one more time.  

There are 2 ways of doing it:

Automatically with rake kill:  
  Just uncomment Rakefile line (Check server:port if match yours):  

  ```
    system "sudo zkCli -server localhost:2181 -cmd deleteall /kamilapp"
  ```

Manually:  
  connect to Your zookeeper cli and run  

  ```
    deleteall /kamilapp
  ```

* ### If any process lost connection to zk

Everything is still working processes will spawn updates and barrier will work as expected.  
But there is no reconection implemented for now.  
If process lost zk connection it should stop working and try to reconnect (it should not work if out of sync !)  
This is on TODO list to implement

* ### There are a lot of __notify__ methods out there

In order for good reviewing what is going on in our app when it is running You can read each process log file 
and can figure out how it is working. You can also compare the times of updates.  
Barrier working. Timeouts. Update blocking e.t.c  


* ### Some things could be better

Of course there is a lot to do with this code. I just need more time:

classes refactoring(split to smaller ones)  
remove ifelse where possible  
add more specs (integration and funcionall) for now only public api is tested.


![Flow](https://bitbucket.org/oberfirer/confsync/raw/d0b3bed5c13ab47585d56f9b603dabc7e27861d8/assets/Flow%20Diagram%20-%20Copy.png)
![Network](https://bitbucket.org/oberfirer/confsync/raw/4dade217b08036ecc2328b12f170e58a6d997ac5/assets/Network%20Diagram.png)